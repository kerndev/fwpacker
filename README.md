# FWPacker

#### 介绍
固件打包工具，设计初衷是用于制作OTA升级包。  
IDE编译生成的BIN文件是原始二进行制文件，  
如果直接用BIN文件来做OTA升级，它没有校验信息，无法对文件完整性做出保证。  
使用此工具可以在文件头部添加校验信息，还可以对文件进行加密。

#### 软件架构
* 开发环境: VS2005
* 开发语言: C

#### 文件结构
| 4BYTE | 4BYTE | 4BYTE | 4BYTE 
| ----- | ----- | ----- | -----  
| HEAD  | HVER  | VID   | PID
| FVER  | ADDR  | SIZE  | CRC
| IMAGE | ..... | ..... | ..... 

* HEAD:  固件标识文件类型
* HVER:  文件头版本
* VID:   厂商ID，32位整数，用于区分不同厂家
* PID:   产品ID，32位整数，用于区分不同产品
* FVER:  固件版本，32位整数(目前自动生成YYYYMMDD格式)
* ADDR:  固件储存地址，决定IMAGE将储存到FLASH中的地址
* SIZE:  固件长度，决定IMAGE的原始长度
* CRC:   固件校验，IMAGE数据校验，用于确保完整性
* IMAGE: 原始BIN文件数据

#### 使用说明

1. 命令行：fwpacker.exe <profile.ini> <input.bin> <output.fw>

| 参数 | 说明 
| -   | -
| profile.ini | 配置文件名
| input.bin   | 待打包的固件文件名
| output.fw   | 打包生成的文件名

注意：如果profile.ini使用相对路径请用"./profile.ini"
