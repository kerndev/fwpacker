/*
* 固件打包工具
* 用法：fwpacker.exe <profile.ini> <input.bin> <output.fw>
* 蒋晓岗<kerndev@foxmail.com>
* 2021.11.12
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <windows.h>
#include "des.h"
#include "crc.h"

#define PACK_HEAD_MAGIC   0x57464758
#define PACK_HEAD_SIZE    32
#define PACK_BLOCK_SIZE   8

struct profile
{
	uint32_t vid;
	uint32_t pid;
	uint32_t addr;
	uint8_t  key[8];
	uint32_t key_valid;
};

struct file_pack
{
	uint32_t magic;
	uint32_t version;
	uint32_t vid;
	uint32_t pid;
	uint32_t ver;
	uint32_t fw_addr;
	uint32_t fw_size;
	uint32_t fw_crc;
	uint8_t  image[4];
};

static uint8_t bin_to_bcd(uint8_t bin)
{
	uint8_t ret;
	ret = bin / 10;
	ret <<= 4;
	ret |= bin % 10;
	return ret;
}

//生成FW_VER
static uint32_t make_fw_ver(void)
{
	uint8_t a,b,c,d;
	uint32_t ret;
	struct tm *date;
	time_t timestamp;
	timestamp = time(NULL) + 28800;
	date = gmtime(&timestamp);
	a   = bin_to_bcd(date->tm_year / 100 + 19);
	b   = bin_to_bcd(date->tm_year % 100);
	c   = bin_to_bcd(date->tm_mon + 1);
	d   = bin_to_bcd(date->tm_mday);
	ret = (a << 24) | (b << 16) | (c << 8) | d;
	return ret;
}

//加密文件
static void encrypt_image(struct file_pack *pack, uint8_t key[8])
{
	uint32_t i;
	uint8_t buf[8];
	for(i=0; i<pack->fw_size; i+=8)
	{
		des_encrypt(buf, &pack->image[i], key);
		memcpy(&pack->image[i], buf, 8);
	}
}

//长度8字节对齐
static uint32_t align_to_block(uint32_t size)
{
	uint32_t rem;
	rem = size % PACK_BLOCK_SIZE;
	if(rem)
	{
		return size + PACK_BLOCK_SIZE - rem;
	}
	return size;
}

//创建文件
struct file_pack *create_pack(char *file_path, struct profile *profile)
{
	FILE *fp;
	long file_size;
	long pack_size;
	struct file_pack *pack;

	//读取文件大小
	fp = fopen(file_path, "rb");
	if(fp == NULL)
	{
		printf("failed to open file: %s\n", file_path);
		return NULL;
	}
	fseek(fp, 0, SEEK_END);
	file_size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	//填充长度至8字节对齐
	pack_size = align_to_block(file_size + PACK_HEAD_SIZE);

	//读取文件数据到内存
	pack = malloc(pack_size);
	if(pack == NULL)
	{
		printf("malloc failed: %d byte\n", pack_size);
		fclose(fp);
		return NULL;
	}
	memset(pack, 0, pack_size);
	fread(pack->image, 1, file_size, fp);
	fclose(fp);
	
	//填写文件头信息
	pack->magic    = PACK_HEAD_MAGIC;
	pack->version  = 1;
	pack->vid      = profile->vid;
	pack->pid      = profile->pid;
	pack->ver      = make_fw_ver();
	pack->fw_addr  = profile->addr;
	pack->fw_size  = file_size;
	pack->fw_crc   = crc32_calc(0, pack->image, pack->fw_size);

	//文件加密
	if(profile->key_valid)
	{
		encrypt_image(pack, profile->key);
	}
	return pack;
}

void delete_pack(struct file_pack *pack)
{
	free(pack);
}

bool write_to_file(struct file_pack *pack, char *file_path)
{
	FILE *fp;
	fp = fopen(file_path, "wb+");
	if(fp == NULL)
	{
		printf("failed to create file: %s\n", file_path);
		return false;
	}
	fwrite(pack, 1, align_to_block(pack->fw_size + PACK_HEAD_SIZE), fp);
	fclose(fp);
	return true;
}

bool read_profile(struct profile *profile, char *file_path)
{
	char str[32];
	GetPrivateProfileStringA("profile", "vid", "0", str, 32, file_path);
	profile->vid = strtoul(str, NULL, 16);
	GetPrivateProfileStringA("profile", "pid", "0", str, 32, file_path);
	profile->pid = strtoul(str, NULL, 16);
	GetPrivateProfileStringA("profile", "addr", "0", str, 32, file_path);
	profile->addr = strtoul(str, NULL, 16);
	memset(str, 0, 32);
	GetPrivateProfileStringA("profile", "key", "0", str, 32, file_path);
	memcpy(profile->key, str, 8);
	profile->key_valid = (strlen(str) == 8);
	return true;
}

void print_banner(void)
{
	printf("Firmware Packer V2.0.1\n");
	printf("jiangxiaogang<kerndev@foxmail.com>\n");
}

void print_usage(void)
{
	printf("usage: fwpacker.exe <profile.ini> <input.bin> <output.fw>\n");
}

void print_pack_info(struct file_pack *pack)
{
	printf("##################################\n");
	printf("vid:  0x%08x\n", pack->vid);
	printf("pid:  0x%08x\n", pack->pid);
	printf("ver:  0x%08x\n", pack->ver);
	printf("base: 0x%08x\n", pack->fw_addr);
	printf("size: 0x%08x\n", pack->fw_size);
	printf("crc:  0x%08x\n", pack->fw_crc);
}

int main(int argc, char **argv)
{
	char *profile_path;
	char *input_path;
	char *output_path;
	struct file_pack *pack;
	struct profile profile;

	print_banner();
	if(argc != 4)
	{
		print_usage();
		return;
	}

	profile_path = argv[1];
	input_path   = argv[2];
	output_path  = argv[3];

	if(!read_profile(&profile, profile_path))
	{
		printf("invalid profile!\n");
		return -1;
	}
	pack = create_pack(input_path, &profile);
	if(pack == NULL)
	{
		printf("create pack failed!\n");
		return -1;
	}
	if(!write_to_file(pack, output_path))
	{
		delete_pack(pack);
		printf("write file failed!\n");
		return -2;
	}
	print_pack_info(pack);
	delete_pack(pack);
	printf("done.\n");
	return 0;
}
